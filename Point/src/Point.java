
public class Point {
 private double x;
 private double y;
 
 Point () {
	 }
 
 Point (double x, double y) {
	 this.x=x;
	 this.y=y;
	  }
 
 public double getX() {
	return x;
}

public void setX(double x) {
	this.x = x;
}

public double getY() {
	return y;
}

public void setY(double y) {
	this.y = y;
}

public String toString() {
		 return "["+ getX() + ", " + getY() + "]";
 }

public void setPosition(double x, double y) {
	this.x= x;
	this.y = y;
	//System.out.println("["+ x + ", " + y + "]");
	}

public void setPosition(Point point) {
	x= point.x;
	y= point.y;
	//System.out.println("["+ x + ", " + y + "]");
}

public boolean samePosition(Point point) {
	if(x == point.x && y == point.y) {
		return true;
	} else { return false;
	}
}

public void move(double dx, double dy) {
	x = x + dx;
	y = y + dy;
	//System.out.println("Nova pozicia bodu je "+ "["+ x + ", " + y + "]");
	}

public Point movedPoint (double dx, double dy) {
	double x = this.x + dx;
	double y = this.y + dy;
	
	Point newPoint = new Point(x, y);
	return newPoint;
}
public double distanceTo(double x, double y) {
	//pytagorova veta
	double b = x - this.x;
	double c = y - this.y;
	double d = b*b + c*c;
	double a = Math.sqrt(d);
	return a;
}
}

